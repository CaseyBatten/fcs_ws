/*
 * The Dealer_fg (full game) node file creates a 52 card deck, shuffles and cuts it just like the Dealer.cpp file
 * The Dealer_fg node will then wait for 3 players to join the game on a playerjoin subscriber
 * Upon three players sending a "join" message, the dealer will begin to deal cards to each player (including itself)
 * Once all cards have been dealt, the dealer will send it's hand to the table_fg node
 */
#include "../include/fcs_pkg/deck.h"
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <string>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int32.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/bool.hpp"
#include "std_msgs/msg/string.hpp"
#include "fcs_pkg/msg/card_data.hpp"

using namespace std::chrono_literals;

//Global Variables
Deck deck;
Card dealerHand[5];
string playerList[4];
int g_playerCount;
int idx;
int turn;
string g_myID = "";
bool played = false;

rclcpp::TimerBase::SharedPtr timer_;

rclcpp::Publisher<std_msgs::msg::Int32>::SharedPtr pub_Int;
rclcpp::Publisher<fcs_pkg::msg::CardData>::SharedPtr pub_Card;
rclcpp::Publisher<fcs_pkg::msg::CardData>::SharedPtr pub_PlayCards;

rclcpp::Subscription<std_msgs::msg::Int32>::SharedPtr sub_Int;
rclcpp::Subscription<std_msgs::msg::String>::SharedPtr sub_String;
rclcpp::Subscription<std_msgs::msg::String>::SharedPtr sub_RequestCard;

//Initialization function that prepares and validates the global deck - can build decks larger than 52
bool initialize(int numDecks){
    g_playerCount = 0;
    g_myID = "Dealer";
    playerList[0] = "Dealer";
    idx = 0;


    //build the deck
    deck = deck.buildDeck(numDecks);

    //Seed the random generator to select a number of shuffles
    srand(time(NULL));
    int numShuffles = rand() % 3 + 3;

    RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"\nNumber of times Shuffled: %d\n",numShuffles);

    for(int i = 0; i < numShuffles; i ++){
        deck.shuffle();
    }

    deck.cut();

    if(deck.size() == (52 * numDecks)){
        return true;
    }

    return false;
}

void DealerActivePub()
{
    auto msg = std_msgs::msg::Int32();
    msg.data = 1;
    pub_Int->publish(msg);
}

void dealCardCallback(const std_msgs::msg::String::SharedPtr playerID){
    if(g_playerCount >= 3){
        if( turn == 0 && idx < 5){
            dealerHand[idx] = deck.draw_from_top();
            idx++;
        }
        else if( turn == 0 && idx >= 5 && !played){
            RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"\nDealer has 5 cards\n");
            played = true;
            for(int i = 0; i < 5; i++){
                auto msg = fcs_pkg::msg::CardData();
                msg.set__id(g_myID);
                msg.set__rank(dealerHand[i].getRank());
                msg.set__suit(dealerHand[i].getSuit());
                pub_PlayCards->publish(msg);
            }
        }

        if(playerList[turn] == playerID->data){
            Card c = deck.draw_from_top();
            auto msg = fcs_pkg::msg::CardData();
            msg.set__id(playerID->data);
            msg.set__rank(c.getRank());
            msg.set__suit(c.getSuit());
            pub_Card->publish(msg);
        }
        turn++;
        if(turn > g_playerCount){
            turn = 0;
        }
    }
}

void playerJoinCallback(const std_msgs::msg::String::SharedPtr playerID){

    if(g_playerCount < 3){
        g_playerCount++;
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"\n%s has joined the game.\n", playerID->data.c_str());
        playerList[g_playerCount] = playerID->data;
    }
    if(g_playerCount >= 3){
        //deal all hands in sequence
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"\nDealer will now deal out all cards...\n");
    }

}

int main(int argc, char **argv){
    bool ok = initialize(1);
    if(ok){
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"\nDeck Made - Waiting for players to join\n");

        rclcpp::init(argc, argv);

        std::shared_ptr<rclcpp::Node> dealer = rclcpp::Node::make_shared("dealer");

        pub_Card = dealer->create_publisher<fcs_pkg::msg::CardData>("card_deal", 10);
        
        pub_PlayCards = dealer->create_publisher<fcs_pkg::msg::CardData>("play_cards", 10);
        
        pub_Int = dealer->create_publisher<std_msgs::msg::Int32>("dealer_active", 10);
        timer_ = dealer->create_wall_timer(1500ms, &DealerActivePub);

        sub_String = dealer->create_subscription<std_msgs::msg::String>("player_join", 10, &playerJoinCallback);
        sub_RequestCard= dealer->create_subscription<std_msgs::msg::String>("request_card", 10, &dealCardCallback);
        
        //send out the dealer active message


        //spin up ros node here
        rclcpp::spin(dealer);
        rclcpp::shutdown();
    }
    else{
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"\nInitialization Error in Dealer.cpp\n");
    }
    return 0;
}