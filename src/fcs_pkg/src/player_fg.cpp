/*
 *  The player fg (full game) node acts as an individual player, collecting 5 cards and then 
 *  passing them to the table once all players have been delt their cards via a "play cards" signal from the dealer
 * 
 */

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int32.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/bool.hpp"
#include "../include/fcs_pkg/deck.h"
#include "fcs_pkg/msg/card_data.hpp"
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <string>


Card playerHand[5];
int loc_ccount = 0;

using namespace std::chrono_literals;

rclcpp::TimerBase::SharedPtr timer_;

rclcpp::Publisher<std_msgs::msg::Int32>::SharedPtr pub_Int;
rclcpp::Publisher<std_msgs::msg::String>::SharedPtr pub_String;
rclcpp::Publisher<std_msgs::msg::String>::SharedPtr pub_RequestCard;
rclcpp::Publisher<fcs_pkg::msg::CardData>::SharedPtr pub_PlayCards;

rclcpp::Subscription<std_msgs::msg::Int32>::SharedPtr sub_Int;
rclcpp::Subscription<fcs_pkg::msg::CardData>::SharedPtr sub_Card;

string g_myID = "";
bool firstTime = true;
bool played = false;

void playHand()
{
    for(int i = 0; i < 5; i++){
        auto msg = fcs_pkg::msg::CardData();
        msg.set__id(g_myID);
        msg.set__rank(playerHand[i].getRank());
        msg.set__suit(playerHand[i].getSuit());
        pub_PlayCards->publish(msg);
    }
    played = true;
}

void requestCard(){
    if(loc_ccount < 5){
        //publish player join of my ID
        auto msg = std_msgs::msg::String();
        msg.data = g_myID;
        pub_RequestCard->publish(msg);
    }
    else if(!played && loc_ccount >= 5){
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Player %s has 5 cards\n", g_myID.c_str());
        playHand();
    }
}

void recvCardCB(const fcs_pkg::msg::CardData::SharedPtr card){
    if(loc_ccount < 5 && card->id == g_myID){
        Card c;
        c.setCard(card->suit, card->rank);
        playerHand[loc_ccount] = c;
        loc_ccount++;
    }
}

void dealerActiveCB(const std_msgs::msg::Int32::SharedPtr valid)
{

    if(valid->data==1 && firstTime){
        //publish player join of my ID
        auto msg = std_msgs::msg::String();
        msg.data = g_myID;
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Asserting %s Active", msg.data.c_str());
        pub_String->publish(msg);
        firstTime = false;
    }
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    std::shared_ptr<rclcpp::Node> player = rclcpp::Node::make_shared("player");

    g_myID = player->get_name();

    sub_Card = player->create_subscription<fcs_pkg::msg::CardData>("card_deal", 10, &recvCardCB);

    sub_Int = player->create_subscription<std_msgs::msg::Int32>("dealer_active", 10, &dealerActiveCB);

    pub_RequestCard = player->create_publisher<std_msgs::msg::String>("request_card", 10);
    timer_ = player->create_wall_timer(100ms, &requestCard);

    pub_String = player->create_publisher<std_msgs::msg::String>("player_join", 10);

    pub_PlayCards = player->create_publisher<fcs_pkg::msg::CardData>("play_cards", 10);

    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Player %s node is now ready to recieve cards.\n", g_myID.c_str());
    rclcpp::spin(player);
    rclcpp::shutdown();
    return 0;
}