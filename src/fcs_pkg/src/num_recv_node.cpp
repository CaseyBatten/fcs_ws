/*
 *  The following node will subscribe to the num_send_node publisher
 *  This node will send a service request to num_send_node requesting a number
 * 
 */
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int32.hpp"
#include "std_srvs/srv/trigger.hpp"
#include <chrono>

using namespace std_srvs::srv;
using namespace std::chrono_literals;

rclcpp::Subscription<std_msgs::msg::Int32>::SharedPtr subscription_;

void subscriberCallback(const std_msgs::msg::Int32::SharedPtr msg)
{
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Subscriber received Num: '%d'", msg->data);
}

int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);


    std::shared_ptr<rclcpp::Node> nodeA = rclcpp::Node::make_shared("num_clien");


    subscription_ = nodeA->create_subscription<std_msgs::msg::Int32>("response_topic", 10, &subscriberCallback);


    rclcpp::Client<Trigger>::SharedPtr client = nodeA->create_client<Trigger>("number_service");

    auto request = std::make_shared<Trigger::Request>();

    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Num recv node is requesting service...");

    while (!client->wait_for_service(1s)) {
        if (!rclcpp::ok()) {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "ERROR in num_recv_node");
        return 0;
        }
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Waiting for service...");
    }

    auto result = client->async_send_request(request);
    
    if (rclcpp::spin_until_future_complete(nodeA, result) ==
        rclcpp::FutureReturnCode::SUCCESS)
    {
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "number_service has responded.");
    } else {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Failed to call service number_service");
    }

    rclcpp::shutdown();
    return 0;
}
