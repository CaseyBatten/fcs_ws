/*
 * The Player.cpp file selects five random, non-repeating cards
 * And adds them to the player hand. It then displays the five cards.
 */
#include "../include/fcs_pkg/deck.h"
#include <stdlib.h>
#include <time.h>
#include <string>
#include <unistd.h>
#include "rclcpp/rclcpp.hpp"

//Global Variables
Card playerHand[5];

string cardPrint(Card c){
    string card ="";

    int r = c.getRank() + 2;
    if( r < 11 ){
        card.append(to_string(r));
    }
    else{
        switch (r){
        case 11:
            card.append("Jack");
            break;
        case 12:
            card.append("Queen");
            break;
        case 13:
            card.append("King");
            break;
        case 14:
            card.append("Ace");
            break;
        default:
            break;
        }
    }

    switch(c.getSuit()){
        case 0:
            card.append(" of Hearts");
            break;
        case 1:
            card.append(" of Clubs");
            break;
        case 2:
            card.append(" of Diamonds");
            break;
        case 3:
            card.append(" of Spades");
            break;
        default:
            break;
    }
    
    return card;
}  


int main(){

    //Of note: section 2.3 did not specify that the player executable needed to pull from the same deck as the dealer
    //However it did state that the dealer and player functions must not relate or communicate
    //Because of this I have chose to randomize the five player cards for this step
    //Future ROS2 steps will maintain a shared deck

    //Sleep added for the sake of clean prints as the dealer and player are unaware of eachother here
    usleep(3000000);

    RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"\nAdding cards to Player Hand...\n");
    

    //Adds five random non-repeating cards to the player hand
    for( int i = 0; i < 5; i++){
        Card c;
        srand(time(NULL));
        int s = rand() % 4;
        int r = rand() % 13;
        c.setCard(s,r);
        bool valid = true;
        for(int j = 0; j < 5; j++){
            if(c.getRank() == playerHand[j].getRank() && c.getSuit() == playerHand[j].getSuit()){
                valid = false;
            }
        }
        if (valid){
            playerHand[i] = c;
        }
        else{ i--; }
    }

    //Shows the cards
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "\nPlayers Hand:\n");
    for(int i = 0; i < 5; i++){
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "%s\n", cardPrint(playerHand[i]).c_str());
    }


    return 0;
}