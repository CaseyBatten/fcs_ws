/*
 * The Dealer.cpp file focuses on generating the 52 card deck for the dealer and player
 * applications to use - it begins by shuffling a random amount of times (between 3 and 5 times),
 * cuts the deck, then begins the game by dealing itself 5 cards.
 */
#include "../include/fcs_pkg/deck.h"
#include <stdlib.h>
#include <time.h>
#include <string>
#include "rclcpp/rclcpp.hpp"

//Global Variables
Deck deck;
Card dealerHand[5];

//Initialization function that prepares and validates the global deck - can build decks larger than 52
bool initialize(int numDecks){
    
    //build the deck
    deck = deck.buildDeck(numDecks);

    //Seed the random generator to select a number of shuffles
    srand(time(NULL));
    int numShuffles = rand() % 3 + 3;

    RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"\nNumber of times Shuffled: %d\n",numShuffles);

    for(int i = 0; i < numShuffles; i ++){
        deck.shuffle();
    }

    deck.cut();

    if(deck.size() == (52 * numDecks)){
        return true;
    }

    return false;
}

string cardPrint(Card c){
    string card ="";

    int r = c.getRank() + 2;
    if( r < 11 ){
        card.append(to_string(r));
    }
    else{
        switch (r){
        case 11:
            card.append("Jack");
            break;
        case 12:
            card.append("Queen");
            break;
        case 13:
            card.append("King");
            break;
        case 14:
            card.append("Ace");
            break;
        default:
            break;
        }
    }

    switch(c.getSuit()){
        case 0:
            card.append(" of Hearts");
            break;
        case 1:
            card.append(" of Clubs");
            break;
        case 2:
            card.append(" of Diamonds");
            break;
        case 3:
            card.append(" of Spades");
            break;
        default:
            break;
    }
    
    return card;
}  

int main(){
    bool ok = initialize(1);

    if(ok){
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"\nDeck Made - Begin the game\n");
        
        for( int i = 0; i < 5; i++){
            dealerHand[i] = deck.draw_from_top();
        }

        RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"\nDealers Hand:\n");
        for(int i = 0; i < 5; i++){
            RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"%s\n",cardPrint(dealerHand[i]).c_str());
        }

    }
    else{
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"\nInitialization Error in Dealer.cpp\n");
    }

    return 0;
}