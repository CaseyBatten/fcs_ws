/*
 *  The following node will publish numbers upon receiving a service request
 * 
 */

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int32.hpp"
#include "std_srvs/srv/trigger.hpp"

#include <stdlib.h>
#include <time.h>

using namespace std_srvs::srv;

rclcpp::Publisher<std_msgs::msg::Int32>::SharedPtr pub_;

void publisherNum(const std::shared_ptr<Trigger::Request>, const std::shared_ptr<Trigger::Response> response)
{
    srand(time(NULL));
    int n = rand() % 10; // Generates a random number to send to Node A
    auto msg = std_msgs::msg::Int32();
    msg.data = n;
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Service request received, publishing %d", msg.data);
    pub_->publish(msg);
    response->success = true;
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    std::shared_ptr<rclcpp::Node> nodeB = rclcpp::Node::make_shared("num_server");

    pub_ = nodeB->create_publisher<std_msgs::msg::Int32>("response_topic", 10);

    rclcpp::Service<Trigger>::SharedPtr service = nodeB->create_service<Trigger>("number_service", &publisherNum);

    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Num Send node is ready to send number.");
    rclcpp::spin(nodeB);
    rclcpp::shutdown();
    return 0;
}