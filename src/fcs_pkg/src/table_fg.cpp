/*
 *  The Table fg (full game) node will communicate with the player_fg and dealer_fg nodes
 *  Once all players have recieved their 5 cards, they all send the table_fg node their hands with a player ID
 *  The table will then print all players hands in order
 *
 */
#include "../include/fcs_pkg/deck.h"
#include <stdlib.h>
#include <string>
#include "rclcpp/rclcpp.hpp"
#include "fcs_pkg/msg/card_data.hpp"
#include "std_msgs/msg/string.hpp"


rclcpp::Subscription<fcs_pkg::msg::CardData>::SharedPtr sub_Card;
rclcpp::Subscription<std_msgs::msg::String>::SharedPtr sub_String;

int num_players = 0;

struct Player{
    string id;
    Card hand[5];
    int idx;
};

Player players[4];

bool gameEnd = false;
int loc_ccount = 0;

string cardPrint(Card c){
    string card ="";

    int r = c.getRank() + 2;
    if( r < 11 ){
        card.append(to_string(r));
    }
    else{
        switch (r){
        case 11:
            card.append("Jack");
            break;
        case 12:
            card.append("Queen");
            break;
        case 13:
            card.append("King");
            break;
        case 14:
            card.append("Ace");
            break;
        default:
            break;
        }
    }

    switch(c.getSuit()){
        case 0:
            card.append(" of Hearts");
            break;
        case 1:
            card.append(" of Clubs");
            break;
        case 2:
            card.append(" of Diamonds");
            break;
        case 3:
            card.append(" of Spades");
            break;
        default:
            break;
    }
    
    return card;
}  

void printAllHands(){
    for(int i = 0; i < 4; i++){
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "%s's Hand:\n", players[i].id.c_str());
        for(int j = 0; j < 5; j++){
            RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "%s", cardPrint(players[i].hand[j]).c_str());
        }
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "\n");
    }
    gameEnd = true;
}

void recvCardCB(const fcs_pkg::msg::CardData::SharedPtr card){
    if(loc_ccount < 20){
        Card c;
        c.setCard(card->suit, card->rank);
        for(int i = 0; i < 4; i++){
            if(card->id == players[i].id){
                players[i].hand[players[i].idx] = c;
                players[i].idx++;
            }
        }
        loc_ccount++;
    }
    if(loc_ccount >=20 && !gameEnd){
        printAllHands();
    }
}

void playerJoinCallback(const std_msgs::msg::String::SharedPtr playerID){
    if(num_players < 4){
        players[num_players].id = playerID->data;
        players[num_players].idx = 0;
        num_players++;
    }

}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    players[0].id = "Dealer";
    players[0].idx = 0;
    num_players = 1;


    std::shared_ptr<rclcpp::Node> table = rclcpp::Node::make_shared("table");

    sub_Card = table->create_subscription<fcs_pkg::msg::CardData>("play_cards", 10, &recvCardCB);
    sub_String = table->create_subscription<std_msgs::msg::String>("player_join", 10, &playerJoinCallback);

    rclcpp::spin(table);
    rclcpp::shutdown();
    return 0;
}