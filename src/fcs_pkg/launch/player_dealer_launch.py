from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    ld = LaunchDescription()
    
    dealer_node = Node(
        package="fcs_pkg",
        executable="dealer_node",
    )
    player_node = Node(
        package="fcs_pkg",
        executable="player_node"
    )

    ld.add_action(dealer_node)
    ld.add_action(player_node)

    return ld