from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    ld = LaunchDescription()
    
    dealer_fg_node = Node(
        package="fcs_pkg",
        executable="dealer_fg_node",
    )
    player1_node = Node(
        package="fcs_pkg",
        executable="player_fg_node",
        name="Player1"
    )
    player2_node = Node(
        package="fcs_pkg",
        executable="player_fg_node",
        name="Player2"
    )
    player3_node = Node(
        package="fcs_pkg",
        executable="player_fg_node",
        name="Player3"
    )
    table_fg_node = Node(
        package="fcs_pkg",
        executable="table_fg_node"
    )

    ld.add_action(dealer_fg_node)
    ld.add_action(player1_node)
    ld.add_action(player2_node)
    ld.add_action(player3_node)
    ld.add_action(table_fg_node)


    return ld