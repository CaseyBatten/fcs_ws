from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    ld = LaunchDescription()
    
    num_send_node = Node(
        package="fcs_pkg",
        executable="num_send",
    )
    num_recv_node = Node(
        package="fcs_pkg",
        executable="num_recv"
    )

    ld.add_action(num_send_node)
    ld.add_action(num_recv_node)

    return ld