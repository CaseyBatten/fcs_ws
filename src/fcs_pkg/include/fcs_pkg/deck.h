/*
 * The Deck Header file contains class
 * and variable definitions for Deck
 */
#include <bits/stdc++.h>
using namespace std;

class Card{
    enum Suit { Hearts, Clubs, Diamonds, Spades };
    enum Rank { N2, N3, N4, N5, N6, N7, N8, N9, N10, J, Q , K, A };

    Suit suit;
    Rank rank;
    public:
        bool setCard(int s, int r){ 
            if ( s >= Hearts && s <= Spades){
                suit = (Suit)s;
                if ( r >= N2 && r <= A ){
                    rank = (Rank)r;
                    return true;
                }
            }
            return false;
        }
        Suit getSuit(){
            return suit;
        }
        Rank getRank(){
            return rank;
        }
};

class Deck{
    vector<Card> cards;

    public:
        // Returns the size of the scalable deck
        int size(){
            return cards.size();
        }

        //Adds card to the deck
        void add(Card c){
            cards.push_back(c);
        }

        // Draws from the top of the deck
        // 52nd would be top card in traditional deck
        // Remove the card from the deck, reducing size
        Card draw_from_top(){
            Card topCard;
            topCard = cards.back();
            cards.pop_back();
            return topCard;
        }

        // Cut the deck, bottom half on top
        void cut(){
            for( int i = 0; i < (int)cards.size()/2; i++ ){
                Card c;
                c = cards.back();
                cards.pop_back();
                cards.insert(cards.begin(), c);
            }
        }

        // Performs a riffle shuffle on the deck
        // Shuffles deck by splitting deck in two, every other
        // card of first half is offset by a card from the second half
        void shuffle(){
            vector<Card> tCards;
            int deckSize = cards.size();
            for(int i = 0; i < deckSize / 2; i++){
                Card c;
                c = cards.back();
                cards.pop_back();
                tCards.insert(tCards.begin(), c);
            }

            int tDeckSize = tCards.size();
            for(int i = 0; i < tDeckSize; i++){
                Card c;
                c = tCards.back();
                tCards.pop_back();
                cards.insert((cards.end()-(i*2)), c);
            }
            
        }

        //Propegates deck, parameter is number of decks (ex: multi deck black jack)
        Deck buildDeck( int n ){
            Deck deck;
            for(int i= 0; i < n; i++){
                for (int j = 0; j < 4; j++){
                    for ( int k = 0; k < 13; k++){
                        Card card;
                        card.setCard(j,k);
                        deck.add(card);
                    }
                }
            }
            return deck;
        }
};

//Global deck declaration
extern Deck deck;