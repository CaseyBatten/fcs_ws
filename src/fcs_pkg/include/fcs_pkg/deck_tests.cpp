#include <gtest/gtest.h>
#include "deck.h"

Deck tDeck;

TEST(deck, add){
    int n = tDeck.size();
    Card c;
    c.setCard(0,0);

    tDeck.add(c); //Deck should increase by 1

    GTEST_ASSERT_GT(tDeck.size(), n);
}

TEST(deck, draw_from_top){
    bool valid = false;
    Card topCard;
    topCard.setCard(0,0);

    //Top card should be equal to card added from add test
    Card c = tDeck.draw_from_top();
    if(c.getRank() == topCard.getRank() && c.getSuit() == topCard.getSuit()){ valid = true; }

    GTEST_ASSERT_EQ(valid, true);

}

TEST(deck, cut){
    bool valid = false;
    /* Create a deck made of:
     * Top Deck -> 5 of Spades, 4 of Diamonds, 
     * 3 of Clubs,  2 of Hearts <- Bottom Deck
     *
     * Cut will create a deck made of:
     * Top Deck -> 3 of Clubs,  2 of Hearts,
     * 5 of Spades, 4 of Diamonds <- Bottom Deck
     */

    Card a, b, c, d;
    a.setCard(0,0);
    tDeck.add(a);
    b.setCard(1,1);
    tDeck.add(b);
    c.setCard(2,2);
    tDeck.add(c);
    d.setCard(3,3);
    tDeck.add(d);

    // Comparison deck
    Deck comp; 
    comp.add(c);
    comp.add(d);
    comp.add(a);
    comp.add(b);

    //Cut the deck, putting the bottom on top
    tDeck.cut();

    if (comp.size() == tDeck.size()){
        for(int i=0; i < tDeck.size(); i++){
            Card j = tDeck.draw_from_top(), k = comp.draw_from_top();

            if( j.getRank() == k.getRank() && j.getSuit() == k.getSuit() ){
                valid = true;
            }
            else{
                valid = false;
                break;
            }
        }
    }

    GTEST_ASSERT_EQ(valid, true);
}

TEST(deck, shuffle){
    bool valid = false;

    Deck comp;
    comp = comp.buildDeck(1); // comparison deck

    tDeck = tDeck.buildDeck(1);
    tDeck.shuffle();
    
    // After a shuffle, the top card should be 3 12 aka the Ace of spades
    // The bottom card should be 0 0, aka the 2 of Hearts

    if (comp.size() == tDeck.size()){
        for(int i=0; i < tDeck.size(); i++){
            if(i == 0){
                //compare the first cards of shuffled and unshuffled decks
                Card j = tDeck.draw_from_top(), k = comp.draw_from_top();
                if( j.getRank() == k.getRank() && j.getSuit() == k.getSuit() ){
                    valid = true;
                }
                else{
                    valid = false;
                    break;
                }
            }
            else if( i == tDeck.size()-1){
                //compare the last cards of shuffled and unshuffled decks
                Card j = tDeck.draw_from_top(), k = comp.draw_from_top();
                if( j.getRank() == k.getRank() && j.getSuit() == k.getSuit() ){
                    valid = true;
                }
                else{
                    valid = false;
                    break;
                }
            }
            else{
                //discard the card
                tDeck.draw_from_top();
            }
        }
    }

    GTEST_ASSERT_EQ(valid, true);

}

int main(int argc, char* argv[]){

    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}