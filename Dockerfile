FROM osrf/ros:foxy-desktop

SHELL [ "/bin/bash", "-c" ]

RUN apt-get update --fix-missing && \
    apt-get install -y git \
                        nano \
                        vim \
                        python3-pip \
                        tmux

RUN mkdir -p fcs_ws/src/fcs_pkg

COPY . /fcs_ws/src/fcs_pkg

RUN source /opt/ros/foxy/setup.sh && \
    cd fcs_ws/ && \
    rosdep install -i --from-path src --rosdistro foxy -y && \
    colcon build

RUN sed --in-place --expression \
      '$isource "fcs_ws/install/setup.bash"' \
      /ros_entrypoint.sh

CMD ["ros2", "launch", "fcs_pkg", "player_dealer_launch.py"]
#CMD ["ros2", "launch", "fcs_pkg", "fcs_fullgame_launcher.py"]