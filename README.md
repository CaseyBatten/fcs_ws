# Fcs_ws README
## Description

This repository contains a ROS2 package titled **fcs_pkg** containing several different nodes and related launch files. The primary launch file is used to create a game of Five-card stud between a single dealer and three players. The other launchers include an isolated dealer/player launcher, and a launcher which demonstrates a standard ROS2 client/service relationship which results in a sender node publishing a random integer to a receiver node.

This repo makes use of a CI/CD pipeline to verify that the underlying data structures and features of the Deck class are functioning as intended. It also will build and run a Docker file executing the isolated dealer/player function. This Docker file can be modified to run the full five-card stud game launcher instead as well.

This ROS2 package also creates a custom ROS message format which contains information about individual cards following this structure:
```
string id   //The id (name) of the player who owns this card
int32 suit  //The suit (clubs, hearts, diamonds, spades) of this card
int32 rank  //The rank (Ace-King) of this Card
```

## Instructions

To utilize the **fcs_pkg** ROS2 package, clone the repository into a desired workspace, and execute the following:
```
source /opt/ros/foxy/setup.sh
cd ~/fcs_ws/
colcon build --packages-select fcs_pkg
```

To execute any of the individual node via **ros2 run** or **ros2 launch**, open a new terminal and enter the following:
```
source ~/fcs_ws/install/setup.bash 

```

To launch the num_service nodes (num_recv_node and num_send_node), execute the following:

```
ros2 launch fcs_pkg num_service_launch.py
```

To launch the dealer and player isolated nodes (dealer_node and player_node), execute the following:
**Note:** This is the launcher currently set up to execute under the Dockerfile included in the repository.
```
ros2 launch fcs_pkg player_dealer_launch.py
```

To launch the full five-card stud game nodes (dealer_fg_node, player_fg_node, and table_fg_node), execute the following:
```
ros2 launch fcs_pkg fcs_fullgame_launcher.py
```

## Google Test

Included in the repostiory's directory **src/fcs_pkg/include** are the primary Deck header file, as well as a google tests program titled **deck_tests.cpp**. This program can be executed using standard CMake functions. These tests are run automatically in the CI/CD pipeline of this project, however to run them locally the user can execute the following:
```
cd ~/fcs_ws/src/fcs_pkg/include/fcs_pkg/
cmake -S . -B build
cmake --build build
./build/deck_tests
```

## Docker

This repository contains a Docker file that can be used to generate a container and test run the ROS2 nodes for the dealer/player isolated nodes. It can also be modified to run the full five card stud game and its related nodes. To change which launcher is executed, the user can comment out **line 25** and uncomment **line 26** of the Dockefile.

To use the included Docker file, first ensure that local sessions of docker engine or docker desktop are running. Also ensure a valid login of the docker sessions. To build and run the included Dockerfile, execute the following:
```
docker build -t fcs_container .
docker run fcs_container
```

